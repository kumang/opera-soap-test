require('dotenv').config()

const axios = require('axios')
const fs = require('fs')

const xmlparser = require('xml-js')

// const payload = `<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Header><OGHeader transactionID="0329878907" primaryLangID="E" timeStamp="2021-03-18T14:06:57.818Z" xmlns="http://webservices.micros.com/og/4.3/Core/"><Authentication><UserCredentials><UserName>HGPS@DEMO</UserName><UserPassword>opera123</UserPassword><Domain>FSDH</Domain></UserCredentials></Authentication></OGHeader></soap:Header><soap:Body><FetchBookingRequest xmlns="http://webservices.micros.com/ows/5.1/Reservation.wsdl"><ConfirmationNumber type="INTERNAL">58024</ConfirmationNumber></FetchBookingRequest></soap:Body></soap:Envelope>`
const payload = `<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <soap:Header>
    <OGHeader transactionID="VANDANATH2" primaryLangID="E" timeStamp="2021-03-19T02:25:11.780Z" xmlns="http://webservices.micros.com/og/4.3/Core/">
      <Origin entityID="KIOSK" systemType="KIOSK"/>
      <Destination entityID="OPERA" systemType="PMS"/>
    </OGHeader>
  </soap:Header>
  <soap:Body>
    <FetchBookingRequest xmlns="http://webservices.micros.com/ows/5.1/Reservation.wsdl">
      <ResvNameId type="INTERNAL">58023</ResvNameId>
      <ResvNameId type="INTERNAL"/>
    </FetchBookingRequest>
  </soap:Body>
</soap:Envelope>`

const start = async () => {
    try {
        let resp = await axios.default.post(`${process.env.KIOSK_URL}/OWS_WS_51/Reservation.asmx`, payload, {
            headers: {
                'Accept-Encoding': 'gzip,deflate',
                'Content-Type': 'text/xml;charset=UTF-8',
                'SOAPAction': '"http://webservices.micros.com/ows/5.1/Reservation.wsdl#FetchBooking"',
                'Connection': 'Keep-Alive'
            }
        })
        // console.log(resp.data)

        const json_data = xmlparser.xml2js(resp.data)
        console.log(JSON.stringify(json_data, null, 2))
    } catch (e) {
        console.log('--error', e.resposne ? e.response.status : e)
        console.log('--error req.headers', e.request ?  e.request._header : '-')
    }
}

start()